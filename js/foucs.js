﻿; (function ($) {
    $.extend({
        'foucs': function (con) {
            var main_width = $('.hero_img').width();
            var main_width_2 = main_width*2;

            var $container = $('#index_c_hero')
                , $imgs = $container.find('li.hero')
            , $leftBtn = $container.find('a.prev')
            , $rightBtn = $container.find('a.next')
            , config = {
                interval: con && con.interval || 5000,
                animateTime: con && con.animateTime || 500,
                direction: con && (con.direction === 'right'),
                _imgLen: $imgs.length
            }
            , i = 0
            , getNextIndex = function (y) { return i + y >= config._imgLen ? i + y - config._imgLen : i + y; }
            , getPrevIndex = function (y) { return i - y < 0 ? config._imgLen + i - y : i - y; }
            , silde = function (d) {
                $imgs.css('z-index', '1');
                $imgs.eq(i).css('padding-left', '5px');
                $imgs.eq(i).css('padding-right', '5px');
                $imgs.eq((d ? getPrevIndex(2) : getNextIndex(2))).css('left', (d ? '-'+main_width_2+'px' : main_width_2+'px'))
                $imgs.animate({
                    'left': (d ? '+' : '-') + '='+main_width+'px'
                }, config.animateTime);
                i = d ? getPrevIndex(1) : getNextIndex(1);

                $imgs.eq(i).css('z-index', '99');
                //$imgs.eq(getNextIndex(1)).css('z-index', '99');
                //$imgs.eq(getPrevIndex(1)).css('z-index', '99');

            }
            , s = setInterval(function () { silde(config.direction); }, config.interval);

            $imgs.eq(i).css('left', 0).end().eq(i + 1).css('left', main_width+'px').end().eq(i - 1).css('left', '-'+main_width+'px');
            
            $container.find('.hero-wrap').add($leftBtn).add($rightBtn).hover(function () { clearInterval(s); }, function () { s = setInterval(function () { silde(config.direction); }, config.interval); });
            $leftBtn.click(function () {
                if ($(':animated').length === 0) {
                    silde(false);
                }
            });
            $rightBtn.click(function () {
                if ($(':animated').length === 0) {
                    silde(true);
                }
            });
        }
    });
}(jQuery));

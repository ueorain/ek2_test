
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') +
      '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  })();

  googletag.cmd.push(function() {
    googletag.defineSlot('/1095084/尋夢園-首頁//彈跳-300x250', [300, 250], 'div-gpt-ad-1441615517159-0').addService(googletag.pubads());
    googletag.defineSlot('/1095084/聊天室上方-468x60', [468, 60], 'div-gpt-ad-1441188902158-0').addService(googletag.pubads());
    googletag.defineSlot('/1095084/首頁320x50_1', [320, 50], 'div-gpt-ad-1419331293567-0').addService(googletag.pubads());
	googletag.defineSlot('/1095084/首頁320x50', [320, 50], 'div-gpt-ad-1419322079108-0').addService(googletag.pubads());
	googletag.defineSlot('/1095084/首頁336x280', [336, 280], 'div-gpt-ad-1419322079108-1').addService(googletag.pubads());
	googletag.defineSlot('/1095084/首頁970x90', [970, 90], 'div-gpt-ad-1419322079108-2').addService(googletag.pubads());
	googletag.defineSlot('/1095084/ek21_home_popup_300250_1', [300, 250], 'div-gpt-ad-1441185135057-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
